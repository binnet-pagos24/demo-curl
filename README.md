# Pagos24 cURL Demo

This project is a demonstration of the use of the cURLs found in the Pagos24
documentation.

## Usage

To use the project just clone this repository and execute the command:

```bash
    npm install
```

When installing the necessary dependencies to run the project, you must execute
the following command:

```bash
    npm run start
```

## Documentation

To know all the endpoints and how they are used we invite you to go to our
[documentation](https://www.pagos24.com/doc).
