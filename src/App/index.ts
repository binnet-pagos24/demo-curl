import express, {Application} from 'express';
import cors from 'cors';
import {createServer} from 'http';
import routes from './routes';

class App {
  public app: Application;

  constructor() {
    this.app = express();
    this.config();
  }

  private config(): void {
    this.app.use(express.json());
    this.app.use(cors({}));
    routes(this.app);
  }
}

export default createServer(new App().app);
