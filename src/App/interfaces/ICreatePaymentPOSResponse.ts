export interface ICreatePaymentPOSResponse {
  id: string;
  amount: number;
  amount_capturable: number;
  amount_received: number;
  application: any;
  application_fee_amount: number | null;
  canceled_at: number | null;
  cancellation_reason: any;
  capture_method: string;
  charges: {
    data: any[];
    has_more: boolean;
    total_count: number;
  };
  client_secret: string | null;
  confirmation_method: string;
  created: number;
  currency: string;
  customer: any;
  description: string | null;
  invoice: any;
  last_payment_error: any;
  livemode: boolean;
  metadata: any;
  next_action: any;
  on_behalf_of: any;
  payment_method: any;
  payment_method_options: any;
  payment_method_types: any[];
  receipt_email: null | string;
  review: any;
  setup_future_usage: any;
  shipping: any;
  source: any;
  statement_descriptor: any;
  statement_descriptor_suffix: any;
  status: string;
  transfer_data: any;
  transfer_group: any;
}
