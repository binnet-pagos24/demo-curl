import {ECardStatus} from '../enums/ECardStatus';
import {ECardType} from '../enums/ECardType';
import {IDefaultRequest} from './IDefaultRequest';

export interface IChangeCardStatusParams extends IDefaultRequest {
  cvc: string;
  expMonth: number;
  expYear: number;
  lastFourDigitsCard: string;
  type: ECardType;
  status: ECardStatus;
  cardId: string;
  sandbox: boolean;
}
