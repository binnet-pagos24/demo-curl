import {ECardholderType} from '../enums/ECardholderType';
import {IDefaultRequest} from './IDefaultRequest';

export interface ICreateCardholderParams extends IDefaultRequest {
  cardholderType: ECardholderType;
  email: string;
  name: string;
  lastNames?: string;
  idNumber: string;
  sector?: string;
  country: string;
  state: string;
  city: string;
  address: string;
  postalCode: string;
  phone: string;
  latitude: string;
  longitude: string;
  invitedBy: string;
  cardholderName?: string;
  sandbox: boolean;
}
