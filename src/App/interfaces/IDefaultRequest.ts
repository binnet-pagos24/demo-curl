export interface IDefaultRequest {
  appId: string;
  secretKey: string;
  tokenAuthorization: string;
}
