import {IDefaultRequest} from './IDefaultRequest';

export interface IActivateCardParams extends IDefaultRequest {
  cvc: string;
  expMonth: number;
  expYear: number;
  lastFourDigitsCard: string;
  cardIdP24: number;
  number: string;
  cardId: string;
  sandbox: boolean;
}
