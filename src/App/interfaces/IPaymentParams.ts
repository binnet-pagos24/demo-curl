import {ELangTypes} from '../enums/ELangTypes';
import {IDefaultRequest} from './IDefaultRequest';

export interface IPaymentParams extends IDefaultRequest {
  payerEmail: string;
  amount: number;
  OTPago: string;
  url: string;
  position: {
    latitud: string;
    longitud: string;
  };
  lang: ELangTypes;
  sandbox: boolean;
  cardId?: string;
}
