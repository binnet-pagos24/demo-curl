export interface IUserCard {
  card_id: string;
  card_number: string;
  card_number_long: string;
  flag: string;
  flag_id: number;
}
