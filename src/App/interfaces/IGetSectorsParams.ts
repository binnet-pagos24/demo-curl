import {ELangTypes} from '../enums/ELangTypes';
import {IDefaultRequest} from './IDefaultRequest';

export interface IGetSectors extends IDefaultRequest {
  language: ELangTypes;
}
