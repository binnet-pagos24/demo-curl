import {IDefaultRequest} from './IDefaultRequest';

export interface IGetCardTransactionsParams extends IDefaultRequest {
  cardId: string;
  sandbox: boolean;
}
