import {ECardType} from '../enums/ECardType';
import {IDefaultRequest} from './IDefaultRequest';
import {IGeolocation} from './IGeolocation';

export interface IChargeCardParams extends IDefaultRequest {
  amount: number;
  cardIdP24: number;
  position: IGeolocation;
  cvc: string;
  expMonth: number;
  expYear: string;
  lastFourDigitsCard: string;
  cardId: string;
  type: ECardType;
  sandbox: boolean;
}
