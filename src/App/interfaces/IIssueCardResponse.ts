export interface IIssueCardResponse {
  brand: string;
  cancellation_reason: null | string;
  cardholder: {
    company: null | string;
    email: string;
    id: string;
    individual: null | string;
    metadata: {};
    name: string;
    status: string;
    type: string;
  };
  id: string;
  created: string;
  currency: string;
  cvc: string;
  exp_month: number;
  exp_year: number;
  last4: string;
  number: string;
  shipping: null;
  spending_controls: null;
  status: string;
  type: string;
  is_expired: boolean;
  cardIdP24: number;
}
