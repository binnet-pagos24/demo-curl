export interface ICreateDisputeResponse {
  amount: string;
  created: string;
  currency: string;
  evidence: {
    reason: string;
    file: {
      base64: string;
      type: string;
    };
    type: {
      fraudulent: {
        explanation: string;
      };
    };
  };
  id: string;
  status: string;
  transaction: string;
}
