import {ELangTypes} from '../enums/ELangTypes';
import {IDefaultRequest} from './IDefaultRequest';

export interface IGetStatesParams extends IDefaultRequest {
  countryIso: string;
  language: ELangTypes;
}
