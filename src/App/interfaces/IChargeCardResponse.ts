export interface IChargeCardResponse {
  amount: number;
  cardId: string;
}
