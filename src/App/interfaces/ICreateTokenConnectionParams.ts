import {IDefaultRequest} from './IDefaultRequest';

export interface ICreateTokenConnectionParam extends IDefaultRequest {
  sandbox: boolean;
}
