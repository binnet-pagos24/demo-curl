import {ECardholderType} from '../enums/ECardholderType';

export interface ICreateCardholderResponse {
  p24Id: string;
  email: string;
  cardHolderId: string;
  created: number;
  name: string;
  cardholderType: ECardholderType;
}
