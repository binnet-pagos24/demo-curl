import {IDefaultRequest} from './IDefaultRequest';

export interface ICancelPaymentParams extends IDefaultRequest {
  paymentIntentId: string;
  sandbox: boolean;
}
