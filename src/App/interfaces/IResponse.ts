/* eslint-disable @typescript-eslint/ban-types */
/* eslint-disable @typescript-eslint/no-explicit-any */
import {EResponseStatus} from '../enums/EResponseStatus';

export interface IResponse<T> {
  statusCode?: EResponseStatus;
  message: string;
  count?: number;
  errorCode?: string;
  errors?: string[] | Object[] | null;
  data?: T;
}
