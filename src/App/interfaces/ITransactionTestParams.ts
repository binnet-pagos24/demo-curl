import {IDefaultRequest} from './IDefaultRequest';

export interface ITransactionTestParams extends IDefaultRequest {
  cvc: string;
  expMonth: number;
  expYear: number;
  number: string;
}
