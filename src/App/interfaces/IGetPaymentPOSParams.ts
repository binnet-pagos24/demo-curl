import {IDefaultRequest} from './IDefaultRequest';

export interface IGetPaymentPOSParams extends IDefaultRequest {
  paymentIntentId: string;
  sandbox: boolean;
}
