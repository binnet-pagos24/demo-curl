import {ELangTypes} from '../enums/ELangTypes';
import {IDefaultRequest} from './IDefaultRequest';

export interface ICapturePaymentParams extends IDefaultRequest {
  paymentIntentId: string;
  posId: string;
  latitude: string;
  longitude: string;
  lang: ELangTypes;
  url: string;
  sandbox: boolean;
}
