import {ECardCancelReasonType} from '../enums/ECardCancelReasonType';
import {IDefaultRequest} from './IDefaultRequest';

export interface ICancelCardParams extends IDefaultRequest {
  cardId: string;
  reason: ECardCancelReasonType;
  cardIdP24: number;
  sandbox: boolean;
}
