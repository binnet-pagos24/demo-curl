import {IDefaultRequest} from './IDefaultRequest';

export interface IClosePOSParams extends IDefaultRequest {
  posId: string;
}
