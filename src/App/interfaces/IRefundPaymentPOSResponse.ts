export interface IRefundPaymentPOSResponse {
  txt_msg: string;
  title_msg: string;
  transaccion: string;
  charge: string;
  amount: number;
  currency: string;
  error: number;
}
