export interface ITransactionTestResponse {
  amount: string;
  created: string;
  currency: string;
}
