import {EEvidenceReason} from '../enums/EEvidenceReason';
import {EImageType} from '../enums/EImageType';
import {EProductType} from '../enums/EProducyType';
import {IDefaultRequest} from './IDefaultRequest';

export interface ICreateDisputeParams extends IDefaultRequest {
  transactionId: string;
  evicence: {
    reason: EEvidenceReason;
    file: {
      base64: string;
      type: EImageType;
    };
    type: {
      fraudulent?: {
        explanation: string;
      };
      duplicate?: {
        explanation: string;
        originalTransactionId: string;
      };
      other?: {
        explanation: string;
        productDescription: string;
        productType: EProductType;
      };
    };
  };
  sandbox: boolean;
}
