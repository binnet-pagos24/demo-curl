export interface ICreateConnectionTokenResponse {
  secret: string;
}
