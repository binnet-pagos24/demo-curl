export interface IOnlinePaymentResponse {
  amount: number;
  envio_sms: boolean;
  title: string;
  trasnsaction_id: string;
  txt_msg: string;
  voucher: string;
}
