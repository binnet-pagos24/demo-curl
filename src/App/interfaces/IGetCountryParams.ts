import {ELangTypes} from '../enums/ELangTypes';
import {IDefaultRequest} from './IDefaultRequest';

export interface IGetCountryParams extends IDefaultRequest {
  language: ELangTypes;
}
