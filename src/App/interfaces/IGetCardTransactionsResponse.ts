export interface IGetCardTransactionsResponse {
  availableBalance: string;
  cardCurrency: string;
  transactions: [
    {
      amount: string;
      card: string;
      cardholder: string;
      created: string;
      currency: string;
      dispute: null | string;
      id: string;
      merchant_amount: number;
      merchant_currency: string;
      merchant_data: {
        category: string;
        city: string;
        country: string;
        name: string;
        network_id: string;
        postal_code: string;
        state: string;
      };
      type: string;
      approved: boolean;
      status: string;
    }
  ];
}
