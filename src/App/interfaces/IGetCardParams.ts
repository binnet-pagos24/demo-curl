import {ECardType} from '../enums/ECardType';
import {IDefaultRequest} from './IDefaultRequest';

export interface IGetCardParam extends IDefaultRequest {
  cardId: string;
  cardholderId: string;
  type: ECardType;
  sandbox: boolean;
}
