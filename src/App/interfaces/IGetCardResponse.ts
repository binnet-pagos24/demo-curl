export interface IGetCardResponse {
  brand: string;
  cancellation_reason: null | string;
  cardholder: {
    company: null | string;
    email: string;
    id: string;
    individual: null | string;
    metadata: {};
    name: string;
    status: string;
    type: string;
  };
  id: string;
  created: string;
  currency: string;
  cvc: string;
  exp_month: number;
  exp_year: number;
  last4: string;
  number: string;
  shipping: {
    carrier: string;
    eta: string;
    status: string;
    tracking_number: null;
    tracking_url: null;
  };
  spending_controls: null;
  status: string;
  type: string;
  is_expired: false;
}
