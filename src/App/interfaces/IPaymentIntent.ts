export interface IPaymentIntent {
  allowed_source_types: ['card_present'];
  amount: number;
  amount_capturable: number;
  amount_received: 0;
  application: null;
  application_fee_amount: null;
  canceled_at: null;
  cancellation_reason: null;
  capture_method: string;
  charges: {
    object: string;
    data: [
      {
        amount: number;
        amount_captured: number;
        amount_refunded: number;
        amount_updates: [];
        application: null;
        application_fee: null;
        application_fee_amount: null;
        authorization_code: string;
        balance_transaction: null;
        billing_details: {
          address: {
            city: null;
            country: null;
            line1: null;
            line2: null;
            postal_code: null;
            state: null;
          };
          email: null;
          name: null;
          phone: null;
        };
        calculated_statement_descriptor: string;
        captured: boolean;
        created: number;
        currency: 'usd';
        customer: null;
        description: string;
        destination: null;
        dispute: null;
        disputed: boolean;
        failure_code: null;
        failure_message: null;
        fraud_details: {};
        id: string;
        invoice: null;
        livemode: false;
        metadata: {};
        object: string;
        on_behalf_of: null;
        order: null;
        outcome: {
          network_status: string;
          reason: null;
          risk_level: string;
          seller_message: string;
          type: string;
        };
        paid: boolean;
        payment_intent: string;
        payment_method: string;
        payment_method_details: {
          card_present: {
            brand: string;
            cardholder_name: null;
            country: string;
            emv_auth_data: string;
            exp_month: number;
            exp_year: number;
            fingerprint: string;
            funding: string;
            generated_card: string;
            last4: string;
            network: string;
            read_method: string;
            receipt: {
              account_type: string;
              application_cryptogram: null;
              application_preferred_name: null;
              authorization_code: null;
              authorization_response_code: string;
              cardholder_verification_method: null;
              dedicated_file_name: null;
              terminal_verification_results: null;
              transaction_status_information: null;
            };
          };
          type: string;
        };
        receipt_email: null;
        receipt_number: null;
        refunded: boolean;
        refunds: {
          object: string;
          data: [];
          has_more: boolean;
          total_count: number;
        };
        review: null;
        shipping: null;
        source: null;
        source_transfer: null;
        statement_descriptor: null;
        statement_descriptor_suffix: null;
        status: string;
        transfer_data: null;
        transfer_group: null;
      }
    ];
    has_more: boolean;
    total_count: number;
  };
  client_secret: string;
  confirmation_method: string;
  created: number;
  currency: number;
  customer: null;
  description: string;
  id: string;
  invoice: null;
  last_payment_error: null;
  livemode: boolean;
  metadata: {};
  next_action: null;
  next_source_action: null;
  object: string;
  on_behalf_of: null;
  payment_method: string;
  payment_method_options: {
    card_present: {};
  };
  payment_method_types: ['card_present'];
  receipt_email: null;
  review: null;
  setup_future_usage: null;
  shipping: null;
  source: null;
  statement_descriptor: null;
  statement_descriptor_suffix: null;
  status: string;
  transfer_data: null;
  transfer_group: null;
}
