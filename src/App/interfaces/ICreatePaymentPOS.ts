import {IDefaultRequest} from './IDefaultRequest';

export interface ICreatePaymentPOSParams extends IDefaultRequest {
  amount: number;
  description: string;
  sandbox: boolean;
}
