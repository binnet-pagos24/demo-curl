import {ELangTypes} from '../enums/ELangTypes';
import {IDefaultRequest} from './IDefaultRequest';

export interface IGetUserCards extends IDefaultRequest {
  userId: string;
  sandbox: boolean;
  lang: ELangTypes;
}
