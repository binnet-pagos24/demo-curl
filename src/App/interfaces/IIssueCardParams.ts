import {ECardType} from '../enums/ECardType';
import {IDefaultRequest} from './IDefaultRequest';
import {IGeolocation} from './IGeolocation';

export interface IIssueCardParams extends IDefaultRequest {
  cardholderId: string;
  type: ECardType;
  p24Id: string;
  position: IGeolocation;
  sandbox: boolean;
}
