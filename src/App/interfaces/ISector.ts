export interface ISector {
  mcc: string;
  name: string;
}
