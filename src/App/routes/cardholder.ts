import {Request, Response, Router} from 'express';
import {CardholderController} from '../controllers/CardholderController';
import {createCardholderSchema} from '../schemas/cardholder';
import schemaValidation from '../utils/middlewares/schemaValidation.middleware';
import * as response from '../utils/response';

const router = Router();

/**
 * @description Create a cardholder
 * @method POST
 */
router.post(
  '/',
  [schemaValidation(createCardholderSchema)],
  async (req: Request, res: Response) => {
    try {
      const cardholderResponse = await CardholderController.createCardholder(
        req.body
      );
      response.success(res, cardholderResponse);
    } catch (error) {
      response.error(res, error);
    }
  }
);

export default router;
