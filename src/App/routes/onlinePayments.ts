import {Request, Router} from 'express';
import {Response} from 'express-serve-static-core';
import {OnlinePaymentsController} from '../controllers/OnlinePaymentsController';
import {
  getUserCardsSchema,
  paymentCreditCardSchema,
  paymentSchema,
} from '../schemas/onlinePayments';
import schemaValidation from '../utils/middlewares/schemaValidation.middleware';
import * as response from '../utils/response';

const router = Router();

/**
 * @description Create payment with Pagos24 balance
 * @method POST
 */
router.post(
  '/credit-card',
  [schemaValidation(paymentCreditCardSchema)],
  async (req: Request, res: Response) => {
    try {
      const paymentResponse =
        await OnlinePaymentsController.paymentWithPagos24CreditCard(req.body);
      response.success(res, paymentResponse);
    } catch (error) {
      response.error(res, error);
    }
  }
);

/**
 * @description Create payment with Pagos24 balance
 * @method POST
 */
router.post(
  '/points',
  [schemaValidation(paymentSchema)],
  async (req: Request, res: Response) => {
    try {
      const paymentResponse =
        await OnlinePaymentsController.paymentWithPagos24Points(req.body);
      response.success(res, paymentResponse);
    } catch (error) {
      response.error(res, error);
    }
  }
);

/**
 * @description Create payment with Pagos24 balance
 * @method POST
 */
router.post(
  '/balance',
  [schemaValidation(paymentSchema)],
  async (req: Request, res: Response) => {
    try {
      const paymentResponse =
        await OnlinePaymentsController.paymentWithPagos24Balance(req.body);
      response.success(res, paymentResponse);
    } catch (error) {
      response.error(res, error);
    }
  }
);

/**
 * @description Get user cards
 * @method POST
 */
router.post(
  '/user-cards',
  [schemaValidation(getUserCardsSchema)],
  async (req: Request, res: Response) => {
    try {
      const cards = await OnlinePaymentsController.getUserCards(req.body);

      response.success(res, cards);
    } catch (error) {
      response.error(res, error);
    }
  }
);

export default router;
