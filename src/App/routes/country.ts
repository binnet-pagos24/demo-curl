import {Router} from 'express';
import {Request, Response} from 'express-serve-static-core';
import {CountryController} from '../controllers/CountryController';
import {getCountriesSchema, getCountryStatesSchema} from '../schemas/country';
import schemaValidation from '../utils/middlewares/schemaValidation.middleware';
import * as response from '../utils/response';

const router = Router();

/**
 * @description Get all countries
 * @method POST
 */
router.post(
  '/',
  [schemaValidation(getCountryStatesSchema)],
  async (req: Request, res: Response) => {
    try {
      const states = await CountryController.getCountries(req.body);
      response.success(res, states);
    } catch (error) {
      response.error(res, error);
    }
  }
);

/**
 * @description Get all countries
 * @method POST
 */
router.post(
  '/',
  [schemaValidation(getCountriesSchema)],
  async (req: Request, res: Response) => {
    try {
      const countries = await CountryController.getCountries(req.body);
      response.success(res, countries);
    } catch (error) {
      response.error(res, error);
    }
  }
);

export default router;
