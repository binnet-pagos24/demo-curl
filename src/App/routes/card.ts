import {Request, Response, Router} from 'express';
import {CardController} from '../controllers/CardController';
import {
  activateCardSchema,
  cancelCardSchema,
  chageCardStatusSchema,
  chargeCardSchema,
  createDisputeSchema,
  getCardSchema,
  getCardTransactionsSchema,
  issueCardSchema,
} from '../schemas/card';
import schemaValidation from '../utils/middlewares/schemaValidation.middleware';
import * as response from '../utils/response';

const router = Router();

/**
 * @description Create a transaction test
 * @method POST
 */
router.post(
  '/transactions/test',
  [schemaValidation(createDisputeSchema)],
  async (req: Request, res: Response) => {
    try {
      const transaction = await CardController.transactionTest(req.body);
      response.success(res, transaction);
    } catch (error) {
      response.error(res, error);
    }
  }
);

/**
 * @description Create a dispute for a transactions
 * @method POST
 */
router.post(
  '/transactions/dispute',
  [schemaValidation(createDisputeSchema)],
  async (req: Request, res: Response) => {
    try {
      const disputeResponse = await CardController.createTransactionDispute(
        req.body
      );
      response.success(res, disputeResponse);
    } catch (error) {
      response.error(res, error);
    }
  }
);

/**
 * @description Get card transactions
 * @method POST
 */
router.post(
  '/transactions',
  [schemaValidation(getCardTransactionsSchema)],
  async (req: Request, res: Response) => {
    try {
      const transactions = await CardController.getCardTransactions(req.body);
      response.success(res, transactions);
    } catch (error) {
      response.error(res, error);
    }
  }
);

/**
 * @description Recharge a card
 * @method POST
 */
router.post(
  '/recharge',
  [schemaValidation(chargeCardSchema)],
  async (req: Request, res: Response) => {
    try {
      const chargeResponse = await CardController.chargeCard(req.body);
      response.success(res, chargeResponse);
    } catch (error) {
      response.error(res, error);
    }
  }
);

/**
 * @description Cancel a card
 * @method POST
 */
router.post(
  '/cancel',
  [schemaValidation(cancelCardSchema)],
  async (req: Request, res: Response) => {
    try {
      const cardResponse = await CardController.getCard(req.body);
      response.success(res, cardResponse);
    } catch (error) {
      response.error(res, error);
    }
  }
);

/**
 * @description Change status a card
 * @method POST
 */
router.post(
  '/status',
  [schemaValidation(chageCardStatusSchema)],
  async (req: Request, res: Response) => {
    try {
      const cardResponse = await CardController.getCard(req.body);
      response.success(res, cardResponse);
    } catch (error) {
      response.error(res, error);
    }
  }
);

/**
 * @description Activate a card
 * @method POST
 */
router.post(
  '/activate',
  [schemaValidation(activateCardSchema)],
  async (req: Request, res: Response) => {
    try {
      const cardResponse = await CardController.getCard(req.body);
      response.success(res, cardResponse);
    } catch (error) {
      response.error(res, error);
    }
  }
);

/**
 * @description Retrive a card
 * @method POST
 */
router.post(
  '/retrive',
  [schemaValidation(getCardSchema)],
  async (req: Request, res: Response) => {
    try {
      const cardResponse = await CardController.getCard(req.body);
      response.success(res, cardResponse);
    } catch (error) {
      response.error(res, error);
    }
  }
);

/**
 * @description Issue a card
 * @method POST
 */
router.post(
  '/issue',
  [schemaValidation(issueCardSchema)],
  async (req: Request, res: Response) => {
    try {
      const issueResponse = await CardController.issueCard(req.body);
      response.success(res, issueResponse);
    } catch (error) {
      response.error(res, error);
    }
  }
);

export default router;
