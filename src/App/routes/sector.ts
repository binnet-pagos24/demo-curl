import {Request, Response, Router} from 'express';
import {SectorController} from '../controllers/SectorController';
import {getSectorsSchema} from '../schemas/sector';
import schemaValidation from '../utils/middlewares/schemaValidation.middleware';
import * as response from '../utils/response';

const router = Router();

/**
 * @description Get all sectors
 * @method POST
 */
router.post(
  '/',
  [schemaValidation(getSectorsSchema)],
  async (req: Request, res: Response) => {
    try {
      const sectors = await SectorController.getSectors(req.body);
      response.success(res, sectors);
    } catch (error) {
      response.error(res, error);
    }
  }
);

export default router;
