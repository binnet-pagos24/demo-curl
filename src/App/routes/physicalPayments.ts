import {Request, Response, Router} from 'express';
import {PhysicalPaymentController} from '../controllers/PhysicalPaymentController';
import {
  cancelPaymentSchema,
  capturePaymentSchema,
  closePOSSchema,
  createPaymentSchema,
  createTokenConnectionSchema,
  getPaymentSchema,
} from '../schemas/physicalPayments';
import schemaValidation from '../utils/middlewares/schemaValidation.middleware';
import * as response from '../utils/response';

const router = Router();

/**
 * @description Get close POS
 * @method POST
 */
router.post(
  '/close',
  [schemaValidation(closePOSSchema)],
  async (req: Request, res: Response) => {
    try {
      const closeResponse = await PhysicalPaymentController.closePos(req.body);

      response.success(res, closeResponse);
    } catch (error) {
      response.error(res, error);
    }
  }
);

/**
 * @description Refund a payment
 * @method POST
 */
router.post(
  '/payment/cancel',
  [schemaValidation(cancelPaymentSchema)],
  async (req: Request, res: Response) => {
    try {
      const payment = await PhysicalPaymentController.cancelPayment(req.body);

      response.success(res, payment);
    } catch (error) {
      response.error(res, error);
    }
  }
);

/**
 * @description Refund a payment
 * @method POST
 */
router.post(
  '/payment/refund',
  [schemaValidation(cancelPaymentSchema)],
  async (req: Request, res: Response) => {
    try {
      const payment = await PhysicalPaymentController.refundPayment(req.body);

      response.success(res, payment);
    } catch (error) {
      response.error(res, error);
    }
  }
);

/**
 * @description Get a payment
 * @method POST
 */
router.post(
  '/payment/retrive',
  [schemaValidation(getPaymentSchema)],
  async (req: Request, res: Response) => {
    try {
      const payment = await PhysicalPaymentController.getPayment(req.body);

      response.success(res, payment);
    } catch (error) {
      response.error(res, error);
    }
  }
);

/**
 * @description Capture a payment
 * @method POST
 */
router.post(
  '/payment/capture',
  [schemaValidation(capturePaymentSchema)],
  async (req: Request, res: Response) => {
    try {
      const capturePaymentResponse =
        await PhysicalPaymentController.createPayment(req.body);

      response.success(res, capturePaymentResponse);
    } catch (error) {
      response.error(res, error);
    }
  }
);

/**
 * @description Create a payment
 * @method POST
 */
router.post(
  '/payment/create',
  [schemaValidation(createPaymentSchema)],
  async (req: Request, res: Response) => {
    try {
      const createPaymentResponse =
        await PhysicalPaymentController.createPayment(req.body);

      response.success(res, createPaymentResponse);
    } catch (error) {
      response.error(res, error);
    }
  }
);

/**
 * @description Create a token connection
 * @method POST
 */
router.post(
  '/connection',
  [schemaValidation(createTokenConnectionSchema)],
  async (req: Request, res: Response) => {
    try {
      const createTokenConenction =
        await PhysicalPaymentController.createTokenConnection(req.body);

      response.success(res, createTokenConenction);
    } catch (error) {
      response.error(res, error);
    }
  }
);

export default router;
