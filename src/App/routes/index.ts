import {Application} from 'express';
import onlinePaymentsRoutes from './onlinePayments';
import physicalPaymentsRoutes from './physicalPayments';
import cardholderRoutes from './cardholder';
import cardRoutes from './card';
import countryRoutes from './country';
import sectorRoutes from './sector';

export default (app: Application) => {
  app.use('/online-payments', onlinePaymentsRoutes);
  app.use('/physical-payments', physicalPaymentsRoutes);
  app.use('/cardholder', cardholderRoutes);
  app.use('/card', cardRoutes);
  app.use('/country', countryRoutes);
  app.use('/sector', sectorRoutes);
};
