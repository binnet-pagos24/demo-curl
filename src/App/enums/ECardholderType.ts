export enum ECardholderType {
  Natural = 'N',
  Legal = 'J',
}
