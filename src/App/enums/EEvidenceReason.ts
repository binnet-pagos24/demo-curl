export enum EEvidenceReason {
  Fraudulent = 'fraudulent',
  Duplicate = 'duplicate',
  Other = 'other',
}
