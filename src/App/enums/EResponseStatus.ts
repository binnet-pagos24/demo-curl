/* eslint-disable no-magic-numbers */
export enum EResponseStatus {
    Success = 200,
    BadRequest = 400,
    Error = 500,
    Created = 201,
    Unauthorize = 401
}