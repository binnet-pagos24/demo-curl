export enum EImageType {
  Jpg = 'JPG',
  Png = 'PNG',
  Jpeg = 'JPEG',
}
