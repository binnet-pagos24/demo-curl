export enum ECardCancelReasonType {
  Lost = 'lost',
  Stolen = 'stolen',
}
