export enum EResponseMessage {
  UnexpectedError = 'Unexpected Error',
  BadRequest = 'Bad Request',
  ServerError = 'Server Error',
  Unauthorize = 'Unauthorize',
}
