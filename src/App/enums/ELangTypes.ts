export enum ELangTypes {
  EsVe = 'es-VE',
  EnUs = 'en-US',
  ptBr = 'pt-BR',
}
