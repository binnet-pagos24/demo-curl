import Joi from 'joi';
import {JoiBoolean, JoiNumber, JoiString} from '../constants/joi.constants';

export const transactionTestSchema = Joi.object({
  cvc: JoiString,
  expMonth: JoiNumber,
  expYear: JoiNumber,
  number: JoiString,
  appId: JoiString,
  secretKey: JoiString,
  tokenAuthorization: JoiString,
});

export const createDisputeSchema = Joi.object({
  transactionId: JoiString,
  evicence: {
    reason: JoiString,
    file: Joi.object({
      base64: JoiString,
      type: JoiString,
    }).required(),
    type: Joi.object({
      fraudulent: Joi.object({
        explanation: JoiString,
      })
        .optional()
        .allow(null),
      duplicate: Joi.object({
        explanation: JoiString,
        originalTransactionId: JoiString,
      })
        .optional()
        .allow(null),
      other: Joi.object({
        explanation: JoiString,
        productDescription: JoiString,
        productType: JoiString,
      })
        .optional()
        .allow(null),
    }).required(),
  },
  sandbox: JoiBoolean,
  appId: JoiString,
  secretKey: JoiString,
  tokenAuthorization: JoiString,
});

export const getCardTransactionsSchema = Joi.object({
  cardId: JoiString,
  sandbox: JoiBoolean,
  appId: JoiString,
  secretKey: JoiString,
  tokenAuthorization: JoiString,
});

export const chargeCardSchema = Joi.object({
  amount: JoiNumber,
  cardIdP24: JoiNumber,
  position: Joi.object({
    latitude: JoiString,
    longitude: JoiString,
  }).required(),
  cvc: JoiString,
  expMonth: JoiNumber,
  expYear: JoiString,
  lastFourDigitsCard: JoiString,
  cardId: JoiString,
  type: JoiString,
  sandbox: JoiBoolean,
  appId: JoiString,
  secretKey: JoiString,
  tokenAuthorization: JoiString,
});

export const cancelCardSchema = Joi.object({
  cardId: JoiString,
  reason: JoiString,
  cardIdP24: JoiNumber,
  sandbox: JoiBoolean,
  appId: JoiString,
  secretKey: JoiString,
  tokenAuthorization: JoiString,
});

export const chageCardStatusSchema = Joi.object({
  appId: JoiString,
  secretKey: JoiString,
  tokenAuthorization: JoiString,
  cvc: JoiString,
  expMonth: JoiNumber,
  expYear: JoiNumber,
  lastFourDigitsCard: JoiString,
  type: JoiString,
  status: JoiString,
  cardId: JoiString,
  sandbox: JoiBoolean,
});

export const activateCardSchema = Joi.object({
  appId: JoiString,
  secretKey: JoiString,
  tokenAuthorization: JoiString,
  cvc: JoiString,
  expMonth: JoiNumber,
  expYear: JoiNumber,
  lastFourDigitsCard: JoiString,
  cardIdP24: JoiNumber,
  number: JoiString,
  cardId: JoiString,
  sandbox: JoiBoolean,
});

export const getCardSchema = Joi.object({
  appId: JoiString,
  secretKey: JoiString,
  tokenAuthorization: JoiString,
  cardId: JoiString,
  cardholderId: JoiString,
  type: JoiString,
  sandbox: JoiBoolean,
});

export const issueCardSchema = Joi.object({
  appId: JoiString,
  secretKey: JoiString,
  tokenAuthorization: JoiString,
  cardholderId: JoiString,
  type: JoiString,
  p24Id: JoiString,
  position: Joi.object({
    latitude: JoiString,
    longitude: JoiString,
  }).required(),
  sandbox: JoiBoolean,
});
