import Joi from 'joi';
import {JoiBoolean, JoiNumber, JoiString} from '../constants/joi.constants';

export const paymentCreditCardSchema = Joi.object({
  appId: JoiString,
  secretKey: JoiString,
  tokenAuthorization: JoiString,
  payerEmail: JoiString,
  amount: JoiNumber,
  OTPago: JoiString,
  url: JoiString,
  position: Joi.object({
    latitud: JoiString,
    longitud: JoiString,
  }).required(),
  lang: JoiString,
  sandbox: JoiBoolean,
  cardId: JoiString,
});

export const paymentSchema = Joi.object({
  appId: JoiString,
  secretKey: JoiString,
  tokenAuthorization: JoiString,
  payerEmail: JoiString,
  amount: JoiNumber,
  OTPago: JoiString,
  url: JoiString,
  position: Joi.object({
    latitud: JoiString,
    longitud: JoiString,
  }).required(),
  lang: JoiString,
  sandbox: JoiBoolean,
});

export const getUserCardsSchema = Joi.object({
  appId: JoiString,
  secretKey: JoiString,
  tokenAuthorization: JoiString,
  userId: JoiString,
  sandbox: JoiBoolean,
  lang: JoiString,
});
