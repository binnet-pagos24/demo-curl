import Joi from 'joi';
import {JoiString} from '../constants/joi.constants';

export const getSectorsSchema = Joi.object({
  appId: JoiString,
  secretKey: JoiString,
  tokenAuthorization: JoiString,
  language: JoiString,
});
