import Joi from 'joi';
import {JoiString} from '../constants/joi.constants';

export const getCountryStatesSchema = Joi.object({
  countryIso: JoiString,
  language: JoiString,
  appId: JoiString,
  secretKey: JoiString,
  tokenAuthorization: JoiString,
});

export const getCountriesSchema = Joi.object({
  appId: JoiString,
  secretKey: JoiString,
  tokenAuthorization: JoiString,
  language: JoiString,
});
