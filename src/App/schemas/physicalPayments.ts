import Joi from 'joi';
import {JoiBoolean, JoiNumber, JoiString} from '../constants/joi.constants';

export const closePOSSchema = Joi.object({
  posId: JoiString,
  appId: JoiString,
  secretKey: JoiString,
  tokenAuthorization: JoiString,
});

export const cancelPaymentSchema = Joi.object({
  appId: JoiString,
  secretKey: JoiString,
  tokenAuthorization: JoiString,
  sandbox: JoiBoolean,
  paymentIntentId: JoiString,
});

export const getPaymentSchema = Joi.object({
  appId: JoiString,
  secretKey: JoiString,
  tokenAuthorization: JoiString,
  sandbox: JoiBoolean,
  paymentIntentId: JoiString,
});

export const capturePaymentSchema = Joi.object({
  appId: JoiString,
  secretKey: JoiString,
  tokenAuthorization: JoiString,
  sandbox: JoiBoolean,
  paymentIntentId: JoiString,
  posId: JoiString,
  latitude: JoiString,
  longitude: JoiString,
  lang: JoiString,
  url: JoiString,
});

export const createPaymentSchema = Joi.object({
  appId: JoiString,
  secretKey: JoiString,
  tokenAuthorization: JoiString,
  sandbox: JoiBoolean,
  amount: JoiNumber,
  description: JoiString,
});

export const createTokenConnectionSchema = Joi.object({
  appId: JoiString,
  secretKey: JoiString,
  tokenAuthorization: JoiString,
  sandbox: JoiBoolean,
});
