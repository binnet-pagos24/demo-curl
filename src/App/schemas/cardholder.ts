import Joi from 'joi';
import {
  JoiBoolean,
  JoiString,
  JoiStringOptional,
} from '../constants/joi.constants';

export const createCardholderSchema = Joi.object({
  cardholderType: JoiString,
  email: JoiString,
  name: JoiString,
  lastNames: JoiStringOptional,
  idNumber: JoiString,
  sector: JoiStringOptional,
  country: JoiString,
  state: JoiString,
  city: JoiString,
  address: JoiString,
  postalCode: JoiString,
  phone: JoiString,
  latitude: JoiString,
  longitude: JoiString,
  invitedBy: JoiString,
  cardholderName: JoiStringOptional,
  sandbox: JoiBoolean,
  appId: JoiString,
  secretKey: JoiString,
  tokenAuthorization: JoiString,
});
