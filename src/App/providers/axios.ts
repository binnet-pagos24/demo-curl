import axios from 'axios';

const axiosIntances = axios.create({
  baseURL: 'https://payments-api.pagos24.app',
  headers: {
    'Content-Type': 'text/plain',
  },
});

export default axiosIntances;
