import {AxiosResponse} from 'axios';
import {IGetUserCards} from '../interfaces/IGetUserCards';
import {IOnlinePaymentResponse} from '../interfaces/IOnlinePaymentResponse';
import {IPaymentParams} from '../interfaces/IPaymentParams';
import {IUserCard} from '../interfaces/IUserCard';
import axiosIntances from '../providers/axios';
import {getPlayloadFormatted} from '../utils/formatted';

export class OnlinePaymentsController {
  /**
   * @description Create payment with Pagos24 balance
   * @param {IPaymentParams} paymentParams
   * @return {Promise<IOnlinePaymentResponse>}
   */
  public static async paymentWithPagos24CreditCard(
    paymentParams: IPaymentParams
  ): Promise<IOnlinePaymentResponse> {
    try {
      const {
        OTPago,
        amount,
        appId,
        lang,
        payerEmail,
        position,
        sandbox,
        secretKey,
        tokenAuthorization,
        url,
        cardId,
      } = paymentParams;

      const paymentFormatted = getPlayloadFormatted(
        {
          OTPago,
          amount,
          lang,
          payerEmail,
          position,
          sandbox,
          url,
          cardId,
        },
        secretKey
      );

      const {data: paymentResponse}: AxiosResponse<IOnlinePaymentResponse> =
        await axiosIntances.post(
          `/payment/credit-card/${appId}`,
          paymentFormatted,
          {
            headers: {
              Authorization: `Basic ${tokenAuthorization}`,
            },
          }
        );

      return Promise.resolve(paymentResponse);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  /**
   * @description Create payment with Pagos24 balance
   * @param {IPaymentParams} paymentParams
   * @return {Promise<IOnlinePaymentResponse>}
   */
  public static async paymentWithPagos24Points(
    paymentParams: IPaymentParams
  ): Promise<IOnlinePaymentResponse> {
    try {
      const {
        OTPago,
        amount,
        appId,
        lang,
        payerEmail,
        position,
        sandbox,
        secretKey,
        tokenAuthorization,
        url,
      } = paymentParams;

      const paymentFormatted = getPlayloadFormatted(
        {
          OTPago,
          amount,
          lang,
          payerEmail,
          position,
          sandbox,
          url,
        },
        secretKey
      );

      const {data: paymentResponse}: AxiosResponse<IOnlinePaymentResponse> =
        await axiosIntances.post(`/payment/points/${appId}`, paymentFormatted, {
          headers: {
            Authorization: `Basic ${tokenAuthorization}`,
          },
        });

      return Promise.resolve(paymentResponse);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  /**
   * @description Create payment with Pagos24 balance
   * @param {IPaymentParams} paymentParams
   * @return {Promise<IOnlinePaymentResponse>}
   */
  public static async paymentWithPagos24Balance(
    paymentParams: IPaymentParams
  ): Promise<IOnlinePaymentResponse> {
    try {
      const {
        OTPago,
        amount,
        appId,
        lang,
        payerEmail,
        position,
        sandbox,
        secretKey,
        tokenAuthorization,
        url,
      } = paymentParams;

      const paymentFormatted = getPlayloadFormatted(
        {
          OTPago,
          amount,
          lang,
          payerEmail,
          position,
          sandbox,
          url,
        },
        secretKey
      );

      const {data: paymentResponse}: AxiosResponse<IOnlinePaymentResponse> =
        await axiosIntances.post(
          `/payment/balance/${appId}`,
          paymentFormatted,
          {
            headers: {
              Authorization: `Basic ${tokenAuthorization}`,
            },
          }
        );

      return Promise.resolve(paymentResponse);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  /**
   * @description Get user cards
   * @param {IGetUserCards} getUserCardsParams
   * @return {Promise<IUserCard[]>}
   */
  public static async getUserCards(
    getUserCardsParams: IGetUserCards
  ): Promise<IUserCard[]> {
    try {
      const {appId, lang, sandbox, secretKey, tokenAuthorization, userId} =
        getUserCardsParams;

      const getCardsFormatted = getPlayloadFormatted(
        {
          userId,
          lang,
          sandbox,
        },
        secretKey
      );

      const {data: userCards}: AxiosResponse<IUserCard[]> =
        await axiosIntances.post(
          `/payment/user-cards/${appId}`,
          getCardsFormatted,
          {
            headers: {
              Authorization: `Basic ${tokenAuthorization}`,
            },
          }
        );

      return Promise.resolve(userCards);
    } catch (error) {
      return Promise.reject(error);
    }
  }
}
