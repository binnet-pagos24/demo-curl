import {AxiosResponse} from 'axios';
import {IGetSectors} from '../interfaces/IGetSectorsParams';
import {ISector} from '../interfaces/ISector';
import axiosIntances from '../providers/axios';
import {getPlayloadFormatted} from '../utils/formatted';

export class SectorController {
  /**
   * @description Get all sectors
   * @param {IGetSectors} getSectorsParams
   * @return {Promise<ISector>}
   */
  public static async getSectors(
    getSectorsParams: IGetSectors
  ): Promise<ISector[]> {
    try {
      const {tokenAuthorization, appId, language, secretKey} = getSectorsParams;

      const getSectorFormatted = getPlayloadFormatted(
        {
          language,
        },
        secretKey
      );

      const {data: sectors}: AxiosResponse<ISector[]> = await axiosIntances.get(
        `/v1.1/sector/${appId}/${getSectorFormatted}`,
        {
          headers: {
            Authorization: tokenAuthorization,
          },
        }
      );

      return Promise.resolve(sectors);
    } catch (error) {
      return Promise.reject(error);
    }
  }
}
