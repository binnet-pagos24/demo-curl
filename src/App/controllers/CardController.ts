import {AxiosResponse} from 'axios';
import {IActivateCardParams} from '../interfaces/IActivateCardParams';
import {ICancelCardParams} from '../interfaces/ICancelCardParams';
import {IChangeCardStatusParams} from '../interfaces/IChangeCardStatusParams';
import {IChargeCardParams} from '../interfaces/IChargeCardParams';
import {IChargeCardResponse} from '../interfaces/IChargeCardResponse';
import {ICreateDisputeParams} from '../interfaces/ICreateDisputeParams';
import {ICreateDisputeResponse} from '../interfaces/ICreateDisputeResponse';
import {IGetCardParam} from '../interfaces/IGetCardParams';
import {IGetCardResponse} from '../interfaces/IGetCardResponse';
import {IGetCardTransactionsParams} from '../interfaces/IGetCardTransacctionsParams';
import {IGetCardTransactionsResponse} from '../interfaces/IGetCardTransactionsResponse';
import {IIssueCardParams} from '../interfaces/IIssueCardParams';
import {IIssueCardResponse} from '../interfaces/IIssueCardResponse';
import {ITransactionTestParams} from '../interfaces/ITransactionTestParams';
import {ITransactionTestResponse} from '../interfaces/ITransactionTestResponse';
import axiosIntances from '../providers/axios';
import {getPlayloadFormatted} from '../utils/formatted';

export class CardController {
  /**
   * @description Create a test transaction
   * @param {ITransactionTestParams} transactionParams
   * @return {Promise<ITransactionTestResponse>}
   */
  public static async transactionTest(
    transactionParams: ITransactionTestParams
  ): Promise<ITransactionTestResponse> {
    try {
      const {
        appId,
        tokenAuthorization,
        cvc,
        expMonth,
        expYear,
        number,
        secretKey,
      } = transactionParams;

      const transactionFormatted = getPlayloadFormatted(
        {
          cvc,
          expMonth,
          expYear,
          number,
        },
        secretKey
      );

      const {
        data: transactionResponse,
      }: AxiosResponse<ITransactionTestResponse> = await axiosIntances.post(
        `/v1.1/transaction/test/${appId}`,
        transactionFormatted,
        {
          headers: {
            Authorization: tokenAuthorization,
          },
        }
      );

      return Promise.resolve(transactionResponse);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  /**
   * @description Create a dispute for a transactions
   * @param {ICreateDisputeParams} createDisputeParams
   * @return {Promise<ICreateDisputeResponse>}
   */
  public static async createTransactionDispute(
    createDisputeParams: ICreateDisputeParams
  ): Promise<ICreateDisputeResponse> {
    try {
      const {
        appId,
        tokenAuthorization,
        evicence,
        sandbox,
        secretKey,
        transactionId,
      } = createDisputeParams;

      const createDispiteFormatted = getPlayloadFormatted(
        {
          evicence,
          sandbox,
          transactionId,
        },
        secretKey
      );

      const {
        data: createDisputeResponse,
      }: AxiosResponse<ICreateDisputeResponse> = await axiosIntances.post(
        `/v1.1/transaction/dispute/${appId}`,
        createDispiteFormatted,
        {
          headers: {
            Authorization: tokenAuthorization,
          },
        }
      );

      return Promise.resolve(createDisputeResponse);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  /**
   * @description Get card transactions
   * @param {IGetCardTransactionsParams} getTransactionsParams
   * @return {Promise<IGetCardTransactionsResponse>}
   */
  public static async getCardTransactions(
    getTransactionsParams: IGetCardTransactionsParams
  ): Promise<IGetCardTransactionsResponse> {
    try {
      const {tokenAuthorization, appId, cardId, sandbox, secretKey} =
        getTransactionsParams;

      const getTransactionsFormatted = getPlayloadFormatted(
        {
          cardId,
          sandbox,
        },
        secretKey
      );

      const {
        data: transactionsResponse,
      }: AxiosResponse<IGetCardTransactionsResponse> = await axiosIntances.post(
        `/v1.1/transaction/list/${appId}`,
        getTransactionsFormatted,
        {
          headers: {
            Authorization: tokenAuthorization,
          },
        }
      );

      return Promise.resolve(transactionsResponse);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  /**
   * @description Charge a card
   * @param {IChargeCardParams} chargeParams
   * @return {Promise<IChargeCardResponse>}
   */
  public static async chargeCard(
    chargeParams: IChargeCardParams
  ): Promise<IChargeCardResponse> {
    try {
      const {
        appId,
        tokenAuthorization,
        amount,
        cardId,
        cardIdP24,
        cvc,
        expMonth,
        expYear,
        lastFourDigitsCard,
        position,
        sandbox,
        secretKey,
        type,
      } = chargeParams;

      const chargeFormatted = getPlayloadFormatted(
        {
          amount,
          cardId,
          cardIdP24,
          cvc,
          expMonth,
          expYear,
          lastFourDigitsCard,
          position,
          sandbox,
          type,
        },
        secretKey
      );

      const {data: chargeResponse}: AxiosResponse<IChargeCardResponse> =
        await axiosIntances.post(
          `/v1.1/card/recharge/${appId}`,
          chargeFormatted,
          {
            headers: {
              Authorization: tokenAuthorization,
            },
          }
        );

      return Promise.resolve(chargeResponse);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  /**
   * @description Cancel a card
   * @param {ICancelCardParams} cancelParams
   * @return {Promise<IGetCardResponse>}
   */
  public static async cancelCard(
    cancelParams: ICancelCardParams
  ): Promise<IGetCardResponse> {
    try {
      const {
        tokenAuthorization,
        appId,
        cardId,
        cardIdP24,
        reason,
        sandbox,
        secretKey,
      } = cancelParams;

      const cancelFormatted = getPlayloadFormatted(
        {
          cardId,
          cardIdP24,
          reason,
          sandbox,
        },
        secretKey
      );

      const {data: cardResponse}: AxiosResponse<IGetCardResponse> =
        await axiosIntances.post(
          `/v1.1/card/cancel/${appId}`,
          cancelFormatted,
          {
            headers: {
              Authorization: tokenAuthorization,
            },
          }
        );

      return Promise.resolve(cardResponse);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  /**
   * @description Change the status a card
   * @param {IChangeCardStatusParams} changeStatusParams
   * @return {Promise<IGetCardResponse>}
   */
  public static async changeCardStatus(
    changeStatusParams: IChangeCardStatusParams
  ): Promise<IGetCardResponse> {
    try {
      const {
        appId,
        tokenAuthorization,
        cardId,
        cvc,
        expMonth,
        expYear,
        lastFourDigitsCard,
        sandbox,
        secretKey,
        status,
        type,
      } = changeStatusParams;

      const changeStatusFormatted = getPlayloadFormatted(
        {
          cardId,
          cvc,
          expMonth,
          expYear,
          lastFourDigitsCard,
          sandbox,
          status,
          type,
        },
        secretKey
      );

      const {data: cardResponse}: AxiosResponse<IGetCardResponse> =
        await axiosIntances.post(
          `/v1.1/card/status/${appId}`,
          changeStatusFormatted,
          {
            headers: {
              Authorization: tokenAuthorization,
            },
          }
        );

      return Promise.resolve(cardResponse);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  /**
   * @description Activate a card
   * @param {IActivateCardParams} activateParams
   * @return {IGetCardResponse}
   */
  public static async activateCard(
    activateParams: IActivateCardParams
  ): Promise<IGetCardResponse> {
    try {
      const {
        appId,
        tokenAuthorization,
        cardId,
        cardIdP24,
        cvc,
        expMonth,
        expYear,
        lastFourDigitsCard,
        number,
        sandbox,
        secretKey,
      } = activateParams;

      const activateParamsFormatted = getPlayloadFormatted(
        {
          cardId,
          cardIdP24,
          cvc,
          expMonth,
          expYear,
          lastFourDigitsCard,
          number,
          sandbox,
        },
        secretKey
      );

      const {data: cardResponse}: AxiosResponse<IGetCardResponse> =
        await axiosIntances.post(
          `/v1.1/card/activate/${appId}`,
          activateParamsFormatted,
          {
            headers: {
              Authorization: tokenAuthorization,
            },
          }
        );

      return Promise.resolve(cardResponse);
    } catch (error) {
      return Promise.resolve(error);
    }
  }

  /**
   * @description Get a card
   * @param {IGetCardParam} getCardParam
   * @return {Promise<IGetCardResponse>}
   */
  public static async getCard(
    getCardParam: IGetCardParam
  ): Promise<IGetCardResponse> {
    try {
      const {
        appId,
        cardId,
        cardholderId,
        sandbox,
        secretKey,
        tokenAuthorization,
        type,
      } = getCardParam;

      const getFormatted = getPlayloadFormatted(
        {
          cardId,
          cardholderId,
          sandbox,
          type,
        },
        secretKey
      );

      const {data: cardResponse}: AxiosResponse<IGetCardResponse> =
        await axiosIntances.post(`/v1.1/card/retrive/${appId}`, getFormatted, {
          headers: {
            Authorization: tokenAuthorization,
          },
        });

      return Promise.resolve(cardResponse);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  /**
   * @description Issue a card
   * @param {IIssueCardParams} issueParams
   * @return {Promise<IIssueCardResponse>}
   */
  public static async issueCard(
    issueParams: IIssueCardParams
  ): Promise<IIssueCardResponse> {
    try {
      const {
        appId,
        cardholderId,
        p24Id,
        position,
        sandbox,
        secretKey,
        tokenAuthorization,
        type,
      } = issueParams;

      const issueFormatted = getPlayloadFormatted(
        {
          cardholderId,
          p24Id,
          position,
          sandbox,
          type,
        },
        secretKey
      );

      const {data: issueResponse}: AxiosResponse<IIssueCardResponse> =
        await axiosIntances.post(`/v1.1/card/create/${appId}`, issueFormatted, {
          headers: {
            Authorization: tokenAuthorization,
          },
        });

      return Promise.resolve(issueResponse);
    } catch (error) {
      return Promise.reject(error);
    }
  }
}
