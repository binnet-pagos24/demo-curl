import {AxiosResponse} from 'axios';
import {ICreateCardholderParams} from '../interfaces/ICreateCardholderParams';
import {ICreateCardholderResponse} from '../interfaces/ICreatecardholderResponse';
import axiosIntances from '../providers/axios';
import {getPlayloadFormatted} from '../utils/formatted';

export class CardholderController {
  /**
   * @description Create a cardholder
   * @param {ICreateCardholderParams} createParams
   * @return {Promise<ICreateCardholderResponse>}
   */
  public static async createCardholder(
    createParams: ICreateCardholderParams
  ): Promise<ICreateCardholderResponse> {
    try {
      const {
        address,
        appId,
        cardholderType,
        city,
        country,
        email,
        idNumber,
        invitedBy,
        latitude,
        longitude,
        name,
        phone,
        postalCode,
        sandbox,
        secretKey,
        state,
        tokenAuthorization,
        cardholderName,
        lastNames,
        sector,
      } = createParams;

      const createFormatted = getPlayloadFormatted(
        {
          address,
          cardholderName,
          cardholderType,
          city,
          country,
          email,
          idNumber,
          invitedBy,
          latitude,
          longitude,
          name,
          phone,
          postalCode,
          sandbox,
          state,
          lastNames,
          sector,
        },
        secretKey
      );

      const {
        data: createCardholderResponse,
      }: AxiosResponse<ICreateCardholderResponse> = await axiosIntances.post(
        `/v1.1/card/cardholder/${appId}`,
        createFormatted,
        {
          headers: {
            Authorization: tokenAuthorization,
          },
        }
      );

      return Promise.resolve(createCardholderResponse);
    } catch (error) {
      return Promise.reject(error);
    }
  }
}
