import {AxiosResponse} from 'axios';
import {ICapturePaymentParams} from '../interfaces/ICapturePaymentParams';
import {ICreateConnectionTokenResponse} from '../interfaces/ICreateConnectionToken';
import {ICreatePaymentPOSParams} from '../interfaces/ICreatePaymentPOS';
import {ICreatePaymentPOSResponse} from '../interfaces/ICreatePaymentPOSResponse';
import {ICreateTokenConnectionParam} from '../interfaces/ICreateTokenConnectionParams';
import {IGetPaymentPOSParams} from '../interfaces/IGetPaymentPOSParams';
import {IOnlinePaymentResponse} from '../interfaces/IOnlinePaymentResponse';
import {IPaymentIntent} from '../interfaces/IPaymentIntent';
import {ICancelPaymentParams} from '../interfaces/ICancelPaymentParams';
import {IRefundPaymentPOSResponse} from '../interfaces/IRefundPaymentPOSResponse';
import axiosIntances from '../providers/axios';
import {getPlayloadFormatted} from '../utils/formatted';
import {IClosePOSResponse} from '../interfaces/IClosePOSResponse';
import {IClosePOSParams} from '../interfaces/IClosePOSParams';

export class PhysicalPaymentController {
  /**
   * @description Get close POS
   * @param {IClosePOSParams} closeParams
   * @return {Promise<IClosePOSResponse[]>}
   */
  public static async closePos(
    closeParams: IClosePOSParams
  ): Promise<IClosePOSResponse[]> {
    try {
      const {appId, posId, secretKey, tokenAuthorization} = closeParams;

      const closeFormatted = getPlayloadFormatted(
        {
          posId,
        },
        secretKey
      );

      const {data: closingResponse}: AxiosResponse<IClosePOSResponse[]> =
        await axiosIntances.post(`/pos/close-pos/${appId}`, closeFormatted, {
          headers: {
            Authorization: tokenAuthorization,
          },
        });

      return Promise.resolve(closingResponse);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  /**
   * @description Refund a payment
   * @param {IRefundParams} refundParams
   * @return {Promise<IRefundPaymentPOSResponse>}
   */
  public static async cancelPayment(
    cancelParams: ICancelPaymentParams
  ): Promise<IPaymentIntent> {
    try {
      const {appId, paymentIntentId, sandbox, secretKey, tokenAuthorization} =
        cancelParams;

      const cancelFormatted = getPlayloadFormatted(
        {
          paymentIntentId,
          sandbox,
        },
        secretKey
      );

      const {data: paymentIntent}: AxiosResponse<IPaymentIntent> =
        await axiosIntances.post(
          `/pos/payment-intent/cancel/${appId}`,
          cancelFormatted,
          {
            headers: {
              Authorization: tokenAuthorization,
            },
          }
        );

      return Promise.resolve(paymentIntent);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  /**
   * @description Refund a payment
   * @param {ICance} refundParams
   * @return {Promise<IRefundPaymentPOSResponse>}
   */
  public static async refundPayment(
    refundParams: ICapturePaymentParams
  ): Promise<IRefundPaymentPOSResponse> {
    try {
      const {appId, tokenAuthorization, paymentIntentId, sandbox, secretKey} =
        refundParams;

      const refundFormatted = getPlayloadFormatted(
        {
          paymentIntentId,
          sandbox,
        },
        secretKey
      );

      const {data: paymentIntent}: AxiosResponse<IRefundPaymentPOSResponse> =
        await axiosIntances.post(
          `/pos/payment-intent/refund/${appId}`,
          refundFormatted,
          {
            headers: {
              Authorization: tokenAuthorization,
            },
          }
        );

      return Promise.resolve(paymentIntent);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  /**
   * @description Get a payment
   * @param {IGetPaymentPOSParams} getPaymentParams
   * @return {Promise<IPaymentIntent>}
   */
  public static async getPayment(
    getPaymentParams: IGetPaymentPOSParams
  ): Promise<IPaymentIntent> {
    try {
      const {appId, paymentIntentId, sandbox, secretKey, tokenAuthorization} =
        getPaymentParams;

      const getPaymentFormatted = getPlayloadFormatted(
        {
          paymentIntentId,
          sandbox,
        },
        secretKey
      );

      const {data: paymentIntent}: AxiosResponse<IPaymentIntent> =
        await axiosIntances.post(
          `/pos/payment-intent/retrive/${appId}`,
          getPaymentFormatted,
          {
            headers: {
              Authorization: tokenAuthorization,
            },
          }
        );

      return Promise.resolve(paymentIntent);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  /**
   * @description Capture a payment
   * @param {ICapturePaymentParams} capturePaymentParams
   * @return {Promise<IOnlinePaymentResponse>}
   */
  public static async capturePayment(
    capturePaymentParams: ICapturePaymentParams
  ): Promise<IOnlinePaymentResponse> {
    try {
      const {
        appId,
        lang,
        latitude,
        longitude,
        paymentIntentId,
        posId,
        sandbox,
        secretKey,
        tokenAuthorization,
        url,
      } = capturePaymentParams;

      const captureFormatted = getPlayloadFormatted(
        {
          paymentIntentId,
          posId,
          latitude,
          longitude,
          lang,
          url,
          sandbox,
        },
        secretKey
      );

      const {data: captureResponse}: AxiosResponse<IOnlinePaymentResponse> =
        await axiosIntances.post(
          `/pos/payment-intent/capture/${appId}`,
          captureFormatted,
          {
            headers: {
              Authorization: tokenAuthorization,
            },
          }
        );

      return Promise.resolve(captureResponse);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  /**
   * @description Create a payment
   * @param {ICreatePaymentPOSParams} createPaymentParams
   * @return {Promise<ICreatePaymentPOSResponse>}
   */
  public static async createPayment(
    createPaymentParams: ICreatePaymentPOSParams
  ): Promise<ICreatePaymentPOSResponse> {
    try {
      const {
        amount,
        description,
        sandbox,
        appId,
        secretKey,
        tokenAuthorization,
      } = createPaymentParams;

      const createPaymentFormatted = getPlayloadFormatted(
        {
          amount,
          description,
          sandbox,
        },
        secretKey
      );

      const {
        data: createPaymentResponse,
      }: AxiosResponse<ICreatePaymentPOSResponse> = await axiosIntances.post(
        `/pos/payment-intent/${appId}`,
        createPaymentFormatted,
        {
          headers: {
            Authorization: tokenAuthorization,
          },
        }
      );

      return Promise.resolve(createPaymentResponse);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  /**
   * @description Create a token connection to the POS
   * @param {ICreateTokenConnectionParam} createTokenParams
   * @return {Promise<ICreateConnectionTokenResponse>}
   */
  public static async createTokenConnection(
    createTokenParams: ICreateTokenConnectionParam
  ): Promise<ICreateConnectionTokenResponse> {
    try {
      const {appId, sandbox, secretKey, tokenAuthorization} = createTokenParams;

      const createFormatted = getPlayloadFormatted({sandbox}, secretKey);

      const {
        data: createTokenResponse,
      }: AxiosResponse<ICreateConnectionTokenResponse> = await axiosIntances.post(
        `/pos/connection/${appId}`,
        createFormatted,
        {
          headers: {
            Authorization: tokenAuthorization,
          },
        }
      );

      return Promise.resolve(createTokenResponse);
    } catch (error) {
      return Promise.reject(error);
    }
  }
}
