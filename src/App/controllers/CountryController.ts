import {AxiosResponse} from 'axios';
import {ICountry} from '../interfaces/ICountry';
import {ICountryState} from '../interfaces/ICountryState';
import {IGetCountryParams} from '../interfaces/IGetCountryParams';
import {IGetStatesParams} from '../interfaces/IGetStatesParams';
import axiosIntances from '../providers/axios';
import {getPlayloadFormatted} from '../utils/formatted';

export class CountryController {
  /**
   * @description Get all states from country
   * @param {IGetStatesParams} getStatesParams
   * @return {Promise<ICountryState[]>}
   */
  public static async getStates(
    getStatesParams: IGetStatesParams
  ): Promise<ICountryState[]> {
    try {
      const {appId, countryIso, language, secretKey, tokenAuthorization} =
        getStatesParams;

      const getStatesFormatted = getPlayloadFormatted(
        {
          language,
          countryIso,
        },
        secretKey
      );

      const {data: States}: AxiosResponse<ICountryState[]> =
        await axiosIntances.get(
          `/v1.1/country/states/${appId}/${getStatesFormatted}`,
          {
            headers: {
              Authorization: tokenAuthorization,
            },
          }
        );

      return Promise.resolve(States);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  /**
   * @description Get all countries
   * @param {IGetCountryParams} getCountryParams
   * @return {ICountry[]}
   */
  public static async getCountries(
    getCountryParams: IGetCountryParams
  ): Promise<ICountry[]> {
    try {
      const {tokenAuthorization, appId, language, secretKey} = getCountryParams;

      const getCountryFormatted = getPlayloadFormatted(
        {
          language,
        },
        secretKey
      );

      const {data: countries}: AxiosResponse<ICountry[]> =
        await axiosIntances.get(
          `/v1.1/country/${appId}/${getCountryFormatted}`,
          {
            headers: {
              Authorization: tokenAuthorization,
            },
          }
        );

      return Promise.resolve(countries);
    } catch (error) {
      return Promise.reject(error);
    }
  }
}
