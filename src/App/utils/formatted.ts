import {HmacSHA256} from 'crypto-js';
import btoa from 'btoa';

/**
 * @description Format the data that will be sent
 * @param {string} data JSON to send
 * @return {string}
 */
export const getPlayloadFormatted = (data: {}, secretKey: string): string => {
  return (
    btoa(JSON.stringify(data)) +
    '.' +
    HmacSHA256(btoa(JSON.stringify(data)), secretKey)
  );
};
