import {Response} from 'express';
import {EResponseMessage} from '../enums/EResponseMessage';
import {EResponseStatus} from '../enums/EResponseStatus';
import {IResponse} from '../interfaces/IResponse';

/**
 * @description Returns a 200 response
 * @param res Express Response
 * @param data
 * @return void
 */
export const success = (res: Response, data: any): void => {
  res.status(EResponseStatus.Success).send(data);
};

/**
 * @description Returns a 200 response
 * @param res {Response} Express Response
 * @param information {IResponse<any>}
 * @return void
 */
export const clientResponse = (
  res: Response,
  information: IResponse<any>
): void => {
  const status = information?.statusCode || EResponseStatus.Success;
  res.status(status).send({
    statusCode: status,
    message: information?.message || 'Peticion exitosa',
    data: information?.data || null,
    errorCode: information?.errorCode || 0,
    errors: information?.errors || null,
  });
};

/**
 * @description Returns a 500 response
 * @param res Express Response
 * @param errors
 * @return void
 */
export const error = (res: Response, errors: any): void => {
  const statusCode: number = errors.statusCode || EResponseStatus.Error;
  let message: string;
  switch (statusCode) {
    case EResponseStatus.BadRequest:
      message = EResponseMessage.BadRequest;
      break;
    case EResponseStatus.Unauthorize:
      message = EResponseMessage.Unauthorize;
      break;
    default:
      message = EResponseMessage.ServerError;
  }
  res.status(statusCode).send({
    statusCode,
    message,
    data: null,
    count: 0,
    errors,
  });
};
