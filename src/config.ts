import {config as dotenv} from 'dotenv';

dotenv();

export default {
  dev: process.env.NODE_ENV !== 'production',
  port: String(process.env.PORT),
  host: String(process.env.HOST),
};
